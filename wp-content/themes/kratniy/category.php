<?php get_header(); ?>

<div class="app-section<?php if ( function_exists('dynamic_sidebar') ) : ?> app-section--two-columns<?php endif; ?> app-section--articles">
    <div class="app-container app-section__container">

        <?php if ( function_exists('dynamic_sidebar') ) : ?>
            <div class="app-section__content">
        <?php endif; ?>

            <h1 class="app-section__header"><?php echo theme_wrap_from_last( single_cat_title( '' , 0), ' ' ); ?></h1>

            <?php if ( have_posts() ) : ?>

                <div class="article-list article-list--posts">
                    <div class="article-list__items">
                        <?php while ( have_posts() ) : the_post() ?>
                        
                        <div class="article-list__item">
                            <div class="article">
                                <?php if ( has_post_thumbnail() ) : ?>
                                <div class="article__image">
                                    <?php the_post_thumbnail('full ', array(
                                        'class' => 'article__image-thumb'
                                    )); ?>
                                </div><!-- e:image -->
                                <?php endif; ?>
                                <div class="article__body">
                                    <div class="article__post"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_the_date(); ?></div><!-- e:post -->
                                    <div class="article__title">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </div><!-- e:post -->
                                    <div class="article__annonce">
                                        <?php the_excerpt(); ?>
                                    </div><!-- e:elem -->
                                    <div class="article__more-link">
                                        <a class="article__link" href="<?php the_permalink(); ?>">Читать дальше<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                                    </div>
                                    <!-- e:link -->
                                </div>
                                <!-- e:body -->
                            </div>
                            <!-- b:article -->
                        </div>
                        <?php endwhile; ?>
                    </div><!-- e:items -->
                </div><!-- b:articles -->
                

                <?php the_posts_pagination( array(
                    'mid_size'  => 2,
                    'prev_next' => true,
                    'prev_text' => '<i class="fa fa-long-arrow-left" aria-hidden="true"></i>',
                    'next_text' => '<i class="fa fa-long-arrow-right" aria-hidden="true"></i>',
                    'screen_reader_text' => 'Навигация по работам'
                ) ); ?>


            <?php else : ?>
                <p><?php echo 'Пока ничего не опубликовано'; ?></p>
            <?php endif; ?>

        <?php if ( function_exists('dynamic_sidebar') ) : ?>
        </div><!-- e:app-section__content -->

        <div class="app-section__aside">
            <div class="most-popular">            
                <?php dynamic_sidebar('right-sidebar'); ?>
            </div>
        </div><!-- e:aside -->
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
