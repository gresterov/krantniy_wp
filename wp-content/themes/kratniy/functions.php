<?php

define( 'THEME_LANG', 'kratniy' );

require_once( 'vendor/autoload.php' );

require_once( 'inc/admin.php' );
require_once( 'inc/carbonfields.php' );
require_once( 'inc/svg_icon.php' );
require_once( 'inc/cyr2lat.php' );

if ( ! function_exists( 'kr2018_setup' ) ) :
    function kr2018_setup() {

        // Remove the version number of WP
        // Warning - this info is also available
        // in the readme.html file in your root directory - delete this file!
        remove_action( 'wp_head', 'wp_generator' );

        // removes EditURI/RSD (Really Simple Discovery) link
        remove_action( 'wp_head', 'rsd_link' );

        // removes wlwmanifest (Windows Live Writer) link
        remove_action( 'wp_head', 'wlwmanifest_link' );

        // removes shortlink
        remove_action( 'wp_head', 'wp_shortlink_wp_head' );

        // removes feed links
        remove_action( 'wp_head', 'feed_links', 2 );

        // removes comments feed
        remove_action( 'wp_head', 'feed_links_extra', 3 );

        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        remove_action( 'wp_print_styles', 'print_emoji_styles' );

        remove_action( 'wp_head', 'rest_output_link_wp_head' );

        remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support( 'title-tag' );

        add_theme_support( 'automatic-feed-links' );
        add_theme_support( 'customize-selective-refresh-widgets' );
        add_theme_support( 'widgets' );

        add_theme_support( 'custom-logo', array(
            'width'       => 213,
            'height'      => 46,
            'flex-height' => true,
            'flex-width'  => true,
            'header-text' => false,
        ));

        add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support( 'post-thumbnails' );


        add_image_size('news-thumb', 370, 280, true);
        add_image_size('news-large', 870, 434, true);

        //Register Menus
        register_nav_menus( array(
            'top_menu' => __( 'Меню в шапке', THEME_LANG )
        ) );
    }
endif;
add_action( 'after_setup_theme', 'kr2018_setup' );

function register_serdcepro_widgets() {
    register_sidebar( array(
        'name'          => 'Боковая колонка',
        'id'            => 'right-sidebar',
        'description'   => 'Виджеты в боковой колонке',
        'class'         => '',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => "</div>\n",
        'before_title'  => '<div class="widget-title">',
        'after_title'   => "</div>\n",
    ) );
}
add_action( 'widgets_init', 'register_serdcepro_widgets' );

function kr2018_register_post_types() {

    register_post_type('cases', array(
        'labels' => array(
            'name'            => 'Кейсы',
            'singular_name'   => 'Кейс',
            'all_items'       => 'Кейсы',
            'add_new'         => 'Добавить кейс',
            'add_new_item'    => 'Добавить новый кейс',
        ),
        'description'         => '',
        'public'              => true,
        'publicly_queryable'  => true,
        'exclude_from_search' => true,
        'show_ui'             => null,
        'show_in_menu'        => true, // показывать ли в меню адмнки
        'show_in_admin_bar'   => null, // по умолчанию значение show_in_menu
        'show_in_nav_menus'   => true,
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-portfolio',
        'hierarchical'        => false,
        // 'supports'            => array(  'title', 'editor', 'thumbnail' ),
        'supports'            => array(  'title' ),
        'taxonomies'          => array(),
        'has_archive'         => true,
        'rewrite'             => array(
            'slug'       => 'cases',
            'with_front' => false
        ),
        'query_var'           => true,
    ) );

}
add_action('init', 'kr2018_register_post_types');


/**
 * Enqueue scripts and styles.
 */
function kr2018_scripts() {

    wp_register_style( 'bootstrap', get_template_directory_uri() . '/bootstrap/css/bootstrap.css' );
    wp_register_style( 'plugins', get_template_directory_uri() . '/css/plugins.min.css' );
    wp_register_style( 'app', get_template_directory_uri() . '/css/style.css' );
    wp_register_style( 'dev', get_template_directory_uri() . '/css/dev.css' );

    wp_enqueue_style( 'bootstrap' );
    wp_enqueue_style( 'plugins' );
    wp_enqueue_style( 'app' );
    wp_enqueue_style( 'dev' );

    wp_deregister_script( 'jquery' );

    wp_register_script( 'pep', get_template_directory_uri() . '/js/vendors/pep-0.4.2.min.js', array(), '1.1.0', true );
    wp_register_script( 'jquery', get_template_directory_uri() . '/js/vendors/jquery-3.2.1.min.js', array(), '2.2.0', true );
    wp_register_script( 'plugins', get_template_directory_uri() . '/js/plugins.min.js', array('jquery'), '1.0.0', true );
    wp_register_script( 'app', get_template_directory_uri() . '/js/app.js', array('jquery'), '1.0.0', true );

    wp_enqueue_script( 'pep' );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'plugins' );
    wp_enqueue_script( 'app' );

}
add_action( 'wp_enqueue_scripts', 'kr2018_scripts' );


function get_theme_logo( $attrs ) {

    $logo_id = get_theme_mod( 'custom_logo' );

    $logo_image = wp_get_attachment_image( $logo_id, 'full', false, $attrs );

    $default_logo = '<img class="' . $attrs['class'] . '" src="' . get_template_directory_uri() . '/images/img/app-logo.png' . '" itemprop="logo">';

    return !empty($logo_image) ? $logo_image : $default_logo;
}



function my_mce4_options($init) {

    $custom_colours = '
        "7bafc7", "Цвет темы",
        "F44336", "Red",
        "E91E63", "Pink",
        "9C27B0", "Purple",
        "673AB7", "Deep Purple",
        "3F51B5", "Indigo",
        "2196F3", "Blue",
        "03A9F4", "Light Blue",
        "00BCD4", "Cyan",
        "009688", "Teal",
        "4CAF50", "Green",
        "8BC34A", "Light Green",
        "CDDC39", "Lime",
        "FFEB3B", "Yellow",
        "FFC107", "Amber",
        "FF9800", "Orange",
        "FF5722", "Deep Orange",
        "795548", "Brown",
        "9E9E9E", "Grey",
        "607D8B", "Blue Grey",
        "000000", "Black",
    ';

    // build colour grid default+custom colors
    $init['textcolor_map'] = '['.$custom_colours.']';

    // change the number of rows in the grid if the number of colors changes
    // 8 swatches per row
    $init['textcolor_rows'] = 3;

    return $init;
}
add_filter('tiny_mce_before_init', 'my_mce4_options');

// поддержка SVG
function my_myme_types($mime_types){
    $mime_types['svg'] = 'image/svg+xml'; 
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);