      <?php if( !is_front_page() ) : ?>
            </main>
            <!-- e:main -->
          </div>
          <!-- b:m-content -->
      <?php endif; ?>


      <footer class="app-footer">
        <div class="app-footer__content container">
          <div class="b-form-wrapper">
            <form action="#" class="b-form b-form--footer">
              <div class="b-form__header">
                <h2 class="b-form__title">Следующий шаг для начала работы</h2>

                <!-- <= title -->
                <div class="b-form__text">Рост начинается с обмена контактами. Оставьте свои контакты и мы свяжемся с Вами в ближайшее время:</div>

                <!-- <= text -->
              </div>

              <!-- <= header -->
              <div class="b-form__content">
                <div class="b-form__item b-form__item--user">
                  <div class="b-form__item-icon">
                    <div class="svg-icon svg-icon--user"><svg class="svg-icon__link"><use xlink:href="#user"></use></svg></div>
                  </div>

                  <!-- <= item-icon -->
                  <input type="text" placeholder="Ваше имя">
                </div>

                <!-- <= item -->
                <div class="b-form__item b-form__item--phone">
                  <div class="b-form__item-icon">
                    <div class="svg-icon svg-icon--phone"><svg class="svg-icon__link"><use xlink:href="#phone"></use></svg></div>
                  </div>

                  <!-- <= item-icon -->
                  <input type="tel" placeholder="номер телефона">
                </div>

                <!-- <= item -->
                <div class="b-form__item b-form__item--button">
                  <button type="submit">Отправить заявку</button>
                </div>

                <!-- <= item -->
              </div>

              <!-- <= content -->
              <div class="b-form__footer">
                <div class="b-form__note">
                  Нажимая на кнопку, вы даете согласие на
                  <a href="#" target="blank">обработку персональных данных</a> и
                  <a href="#" target="blank">соглашаетесь c политикой конфиденциальности</a>
                </div>

                <!-- <= note -->
              </div>

              <!-- <= footer -->
            </form> <!-- /b-form -->
          </div> <!-- /b-form-wrapper -->
        </div>

        <!-- e:content -->
      </footer>

      <!-- b:app-footer -->
    </div>

    <!-- b:wrapper -->
    
    <!--
    <script src="js/vendors/pep-0.4.2.min.js" async=""></script>
    <script src="js/vendors/jquery-3.2.1.min.js"></script>
    <script src="js/plugins.min.js"></script>
    <script src="js/app.js"></script>
    -->

    <?php get_template_part('template-parts/popup', 'form'); ?>

    <?php wp_footer(); ?>
  </body>

  <!-- b:w-page -->
</html>
