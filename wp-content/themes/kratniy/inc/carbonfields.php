<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;


function crb_load() {
    \Carbon_Fields\Carbon_Fields::boot();
}
add_action( 'after_setup_theme', 'crb_load' );


function crb_get_gmaps_api_key( $current_key ) {
    return 'AIzaSyA0HoTAdhItKayUeV3yP_SFVzHKCrE1d9E';
}
add_filter( 'carbon_fields_map_field_api_key', 'crb_get_gmaps_api_key' );


function crb_attach_theme_options() {

    Container::make( 'theme_options', 'theme_settings', __( 'Настройки темы', THEME_LANG ) )

        /* Общие настройки */
        ->add_tab( __( 'Общие настройки', THEME_LANG ), [
            Field::make( 'separator', 'logo_separator', __( 'Лого', THEME_LANG ) ),
            Field::make( 'image', 'logo_white_ru', __( 'Лого белое (рус.)', THEME_LANG ) )
                ->set_width( 100 ),

            Field::make( 'image', 'callback_poster_ru', __( 'Изображение формы (Рус.)', THEME_LANG ) )
                ->set_width( 100 ),

            Field::make( 'file', 'callback_video_ru', __( 'Видео формы (Рус.)', THEME_LANG ) )
                ->set_help_text( 'Видео в формате mp4' )
                ->set_width( 100 ),
           
            Field::make( 'text', 'callback_link_ru', __( 'Шорткод формы (рус.)', THEME_LANG ) )
                ->set_help_text('Contact Form 7')
                ->set_width( 100 ),

            Field::make( 'textarea', 'intro_subtitle_en', __( 'Подзаголовок (англ.)', THEME_LANG ) )
                ->set_width( 100 ),

            Field::make( 'complex', 'socials_ru', __( 'Список соц. сетей (рус.)', THEME_LANG ) )
                ->add_fields( [
                    Field::make( 'text', 'title', __( 'Название', THEME_LANG ) ),
                    Field::make( 'text', 'url', __( 'Ссылка', THEME_LANG ) ),
                    Field::make( 'image', 'icon', __( 'Иконка', THEME_LANG ) ),
                ] )
                ->set_layout( 'tabbed-vertical' )
                ->set_header_template( '<%= title %>' )
                ->set_width( 50 ),

            Field::make( 'association', 'projects_main_ru', __( 'Вывести на главной из списка кейсов (рус.)', THEME_LANG ) )
                ->set_max( 10 )
                ->set_types( array(
                    array(
                        'type' => 'post',
                        'post_type' => 'briefcases',
                    ),
                ) ),

            Field::make( 'map', 'address_map_ru', 'Расположение на карте (рус.)' )
                ->set_position( 56.836758, 60.62578759999997, 16 )
                ->set_width( 100 )
                ->set_help_text( 'Перетащите значок на карте для выбора расположения' ),

            Field::make( 'header_scripts', 'crb_header_script', __( 'Скрипты в шапке сайта', THEME_LANG ) )
                ->set_help_text( 'Google Analitics, Яндекс.Метрика и т.д. и т.п.' ),
            Field::make( 'footer_scripts', 'crb_footer_script', __( 'Скрипты в подвале сайта', THEME_LANG ) )
                ->set_help_text( 'Google Analitics, Яндекс.Метрика и т.д. и т.п.' )

        ] ) /* Общие настройки[end] */
        
        
        /* Вступление */
        ->add_tab( __( 'Вступление', THEME_LANG ), [
            Field::make( 'separator', 'intro_header_separator', __( 'Заголовки', THEME_LANG ) ),
        ] );/* Вступление[end] */


    Container::make( 'post_meta', 'briefcases_opts', __( 'Настройки темы', THEME_LANG ) )
        ->add_fields( [
            Field::make( 'text', 'briefcases_title', __( 'Заголовок', THEME_LANG ) ),
        ] )
        ->set_context( 'carbon_fields_after_title' )
        ->set_priority( 'high' )
        ->show_on_post_type( 'briefcases' );

}
add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );