<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'gresterov_kratniy');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'gj71bQVqc0r&m* TBKSNC2kz0+e8Z^?Y}#saPlb5([RE)-qa<v@.{M^xV>XBKVn9');
define('SECURE_AUTH_KEY',  '0.[/_1i]:reo:+4tID_X<.0q4b7-_&!AlM(jOZ<4a!Q)_>Y.tk-jxKeF7#KE;4W8');
define('LOGGED_IN_KEY',    '/Q]#|(<P4 Z]V4j<NG.-tktQ`uE0_sdf/]9zh !KmnBm@y=BBX}x&-j*.Y7`!F`9');
define('NONCE_KEY',        'C)!sH!+neS&};0VZ}CCl%*u=uXJmnuo>j:.R$YS_|:VSG,#Wfz0LG@uCm>`g:F+:');
define('AUTH_SALT',        'U-,T8^?%t>L+rSk8oFzN`1?.P?/4b<@$ ()7OT/d3r[=Xd6Kplz0A*M.Be:qDazT');
define('SECURE_AUTH_SALT', 'm}4pIFw}MO:*rc~rP[2;!QHB.=Bu~!d_|v^s{Jr@#c!&Y<6NZJ$wX|!RlkAO:>VB');
define('LOGGED_IN_SALT',   'UbyW.$1al8,+3VCf|XvP_VQBZCXP3:aX4x74i3YB}~rkQay=O,ljEi(.]rnTN9D&');
define('NONCE_SALT',       '5NYjGzt;aDw=J6ZZ 1[nU.6`7vlT%74%D|&#IHuDYpBg+Cc%,@? ss`ON*exb9jQ');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'kr2018_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
