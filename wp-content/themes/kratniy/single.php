 <?php get_header(); ?>

<div class="app-section app-section--two-columns app-section--articles">
    <div class="app-container app-section__container">

        <div class="app-section__content">

            <?php if ( isset(get_the_category()[0]->cat_name) ) : ?>
                <h1 class="app-section__header"><?php echo theme_wrap_from_last( get_the_category()[0]->cat_name , ' ' ); ?></h1>
            <?php endif;?>
            
            <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post() ?>
            <div class="article article--datailed">
                <?php if ( has_post_thumbnail() ) : ?>
                <div class="article__image">
                    <?php the_post_thumbnail('news-large', array(
                        'class' => 'article__image-thumb'
                    )); ?>
                </div><!-- e:image -->
                <?php endif; ?>
                <div class="article__body">
                    <div class="article__post"><i class="fa fa-calendar" aria-hidden="true"></i><?php echo get_the_date(); ?></div><!-- e:post -->
                    <div class="article__title"><?php the_title(); ?></div><!-- e:post -->
                    <div class="article__body">
                        <?php the_content(); ?>
                    </div><!-- e:elem -->
                </div><!-- e:body -->
            </div>
            <?php endwhile; ?>
            <?php endif; ?>

            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
            <script src="//yastatic.net/share2/share.js"></script>
            <div class="socials socials--ex2">
                <div class="socials__header">Нравится статья? Поделитесь ею со своими друзьями!</div>
                <div class="ya-share2" id="ya-share2" data-services="gplus,facebook,twitter,odnoklassniki,vkontakte"></div>
            </div>
        </div>

        <?php if ( function_exists('dynamic_sidebar') ) : ?>
        <div class="app-section__aside">
            <div class="most-popular">            
                <?php dynamic_sidebar('right-sidebar'); ?>
            </div>
        </div><!-- e:aside -->
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
