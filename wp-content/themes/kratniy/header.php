<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="cleartype" content="on">
    <meta name="theme-color" content="#29A41C">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="MobileOptimized" content="320">
    <meta name="HandheldFriendly" content="True">

    <!-- 
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="css/plugins.min.css">
    <link rel="stylesheet" href="css/style.css">
    -->

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <?php get_template_part('template-parts/svg-sprite'); ?>

    <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <div class="b-background">
      <div class="b-background__image">
        <img src="images/img/bg1.jpg" alt="">
      </div>

      <!-- <= image -->
    </div> <!-- /b-background -->


    <div class="app__wrapper ">
      <header class="app-header">
        <div class="app-header__content container">
          <div class="app-header__inner row align-items-center justify-content-between">
            <div class="app-header__logo col-5 col-sm-4 col-lg-2 col-xl-3">
              <a href="/">
                <img src="images/img/logo.png" alt="кратный рост" title="кратный рост">
              </a>
            </div>

            <!-- <= logo -->
            <div class="app-header__popup-button mobile-only col-5">
              <a href="#">
                <span class="svg-icon svg-icon--menu"><svg class="svg-icon__link"><use xlink:href="#menu"></use></svg></span>
                <span class="svg-icon svg-icon--close"><svg class="svg-icon__link"><use xlink:href="#close"></use></svg></span>
              </a>
            </div>

            <!-- <= popup-button -->
            <div class="app-header__popup col-lg-10 col-xl-9">
              <div class="app-header__popup-inner container">
                <div class="app-header__popup-content row align-items-lg-center justify-content-lg-between">
                  <nav class="app-header__menu col-lg-auto">
                    <ul>
                      <li>
                        <a href="#">Услуги</a>
                      </li>
                      <li>
                        <a href="#">Как работает</a>
                      </li>
                      <li>
                        <a href="#">Результаты</a>
                      </li>
                      <li>
                        <a href="#">Кейсы</a>
                      </li>
                      <li>
                        <a href="#">Контакты</a>
                      </li>
                    </ul>
                  </nav>
                  <div class="app-header__phone col-lg-auto">
                    <a href="tel:+7(912)691-78-10">+7 (912) 691-78-10</a>
                    <a href="#" class="callbackPopup">Заказать обратный звонок</a>
                  </div>
                </div>

                <!-- <= popup-content -->
              </div>
            </div>

            <!-- <= popup -->
          </div>

          <!-- <= inner -->
        </div>
      </header>

      <!-- b:app-header -->
      <div class="b-site-path site-block">
        <div class="b-site-path__content container">
          <a href="/index.html">Главная</a>
          <span class="b-site-path__separator"></span>

          <!-- <= separator -->
          <a href="/cases.html">Кейсы</a>
          <span class="b-site-path__separator"></span>

          <!-- <= separator -->
          Pilots One
        </div>
        <!-- <= content -->
      </div> <!-- /b-site-path -->

      <div class="b-information site-block js-navigation" data-title="Главная" id="information">
        <div class="b-information__content container">
          <h1 class="b-information__title site-title site-title--left">
            <span>Pilots One</span>
          </h1>

          <!-- <= title -->
          <div class="b-information__text">Комплексная упаковка трансферной компании премиум-класса в Санкт-Петербурге.
            <br>
            <br>Компания занимается организацией трансферных услуг бизнес и премиум-класса в Санкт-Петербурге. Работа в формате B2B.</div>

          <!-- <= text -->
        </div>

        <!-- <= content -->
      </div> <!-- /b-information -->

      <?php if( !is_front_page() ) : ?>
          <div class="app__content container">
            <main class="app__main">
      <?php endif; ?>