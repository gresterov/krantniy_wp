<?php get_header(); ?>
<div class="b-h1-block">
    <div class="b-h1-block__in container">
      <h1 class="b-h1-block__title h1"><?= the_title(); ?></h1>
      <!-- <= title -->
    </div>
    <!-- <= in -->
  </div> <!-- /b-h1-block -->

  <div class="b-text container"><?= the_content(); ?></div> <!-- /b-text -->
<?php get_footer(); ?>
