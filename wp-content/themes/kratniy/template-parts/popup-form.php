<div class="b-popup-form">
  <div class="b-popup-form__in">
    <div class="b-popup-form__box container">
      <div class="b-popup-form__close">
        <span class="svg-icon svg-icon--close"><svg class="svg-icon__link"><use xlink:href="#close"></use></svg></span>
      </div>
      <!-- <= close -->

      <?php 
        $poster = wp_get_attachment_image_url(  '' , 'full' );
        $video = wp_get_attachment_url( '' );

       ?> 

      <?php if ( $video || $poster ) : ?>
      <div class="b-popup-form__video-wr">
        <video class="video" muted="muted" poster="<?= $poster; ?>">
          <source src="<?= $video; ?>" type="video/mp4">
        </video>
      </div>
      <?php endif; ?>

      <!-- <= video -->
      <div class="b-popup-form__form-wr">
        <div class="b-form-popup">
          <?php if( !empty($callback_title = '' )) : ?>
            <div class="b-form-popup__title2 mini-title"><?= $callback_title; ?></div>
          <?php endif; ?>

          <!-- <= item -->

          <?php if( !empty($callback_header = '' )) : ?>
            <div class="b-form-popup__title"><?= $callback_header; ?></div>
          <?php endif; ?>

          <!-- <= item -->

          <?php if( $callback_link = carbon_get_theme_option( 'callback_link_ru' ) ) : ?>
            <?= do_shortcode( $callback_link ); ?>
          <?php endif; ?>


        </div> <!-- /a-form -->
      </div>

      <!-- <= form-wr -->
    </div>

    <!-- <= box -->
  </div>

  <!-- <= in -->
</div> <!-- /b-popup-form -->