'use strict';

/**
 * Подключение JS файлов которые начинаются с подчеркивания
 */
/**
 * Возвращает функцию, которая не будет срабатывать, пока продолжает вызываться.
 * Она сработает только один раз через N миллисекунд после последнего вызова.
 * Если ей передан аргумент `immediate`, то она будет вызвана один раз сразу после
 * первого запуска.
 */
function debounce(func, wait, immediate) {

    var timeout = null,
        context = null,
        args = null,
        later = null,
        callNow = null;

    return function () {

        context = this;
        args = arguments;

        later = function later() {

            timeout = null;
            if (!immediate) {
                func.apply(context, args);
            }
        };
        callNow = immediate && !timeout;

        clearTimeout(timeout);
        timeout = setTimeout(later, wait);

        if (callNow) {
            func.apply(context, args);
        }
    };
}

// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
// http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
// requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
// MIT license

;(function () {
    var lastTime = 0,
        vendors = ['ms', 'moz', 'webkit', 'o'],
        x = void 0,
        currTime = void 0,
        timeToCall = void 0,
        id = void 0;

    for (x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
        window.requestAnimationFrame = window[vendors[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame = window[vendors[x] + 'CancelAnimationFrame'] || window[vendors[x] + 'CancelRequestAnimationFrame'];
    }

    if (!window.requestAnimationFrame) {

        window.requestAnimationFrame = function (callback) {

            currTime = new Date().getTime();
            timeToCall = Math.max(0, 16 - (currTime - lastTime));
            id = window.setTimeout(function () {
                callback(currTime + timeToCall);
            }, timeToCall);

            lastTime = currTime + timeToCall;

            return id;
        };
    }

    if (!window.cancelAnimationFrame) {

        window.cancelAnimationFrame = function (id) {
            clearTimeout(id);
        };
    }
})();
;(function () {

    // Test via a getter in the options object to see if the passive property is accessed

    var supportsPassiveOpts = null;

    try {
        supportsPassiveOpts = Object.defineProperty({}, 'passive', {
            get: function get() {
                window.supportsPassive = true;
            }
        });
        window.addEventListener('est', null, supportsPassiveOpts);
    } catch (e) {}

    // Use our detect's results. passive applied if supported, capture will be false either way.
    //elem.addEventListener('touchstart', fn, supportsPassive ? { passive: true } : false);
})();
function getSVGIconHTML(name, tag, attrs) {

    if (typeof name === 'undefined') {
        console.error('name is required');
        return false;
    }

    if (typeof tag === 'undefined') {
        tag = 'div';
    }

    var classes = 'svg-icon svg-icon--<%= name %>';

    var iconHTML = ['<<%= tag %> <%= classes %>>', '<svg class="svg-icon__link">', '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<%= name %>"></use>', '</svg>', '</<%= tag %>>'].join('').replace(/<%= classes %>/g, 'class="' + classes + '"').replace(/<%= tag %>/g, tag).replace(/<%= name %>/g, name);

    return iconHTML;
}

/* ^^^
 * JQUERY Actions
 * ========================================================================== */
$(function () {

    'use strict';

    /**
     * определение существования элемента на странице
     */

    $.exists = function (selector) {
        return $(selector).length > 0;
    };

    /**
     * [^_]*.js - выборка всех файлов, которые не начинаются с подчеркивания
     */
    $('.app-header__popup-button').on('click', function (event) {
        event.preventDefault();
        $('.app-header').toggleClass('js-menu-opened');
    });

    $(window).on('scroll', function (event) {

        var headerBlock = $('.app-header');
        var headerBlockHeight = headerBlock.outerHeight();
        var headerBlockOffset = headerBlock.offset().top;

        if (headerBlockOffset > headerBlockHeight) {
            headerBlock.addClass('js-fixed');
        } else {
            headerBlock.removeClass('js-fixed');
        }
    }).trigger('scroll');

    $(window).on('resize', function (event) {

        if ($('.app-header').hasClass('js-fixed')) {
            $('body').removeAttr('style');
        } else {
            var topPadding = $('.app-header').outerHeight();
            $('body').css('padding-top', topPadding);
        }
    });
    $('.b-cases-tabs').responsiveTabs({
        rotate: false,
        animation: 'slide',
        startCollapsed: 'accordion',
        collapsible: 'accordion',
        setHash: false
    });

    var $siteWrapper = $('.app__wrapper'),
        $callBackBtn = $('.callbackPopup'),
        $popupForm = $('.b-popup-form'),
        $closePopup = $('.b-popup-form__close');

    $callBackBtn.on('click', function (e) {
        e.preventDefault();

        $siteWrapper.addClass('blockBlur');
        $popupForm.addClass('opened');
        $popupForm.fadeIn(600);

        if (window.matchMedia('(min-width:940px)').matches) {
            $popupForm.find('video').off('click', function () {
                this[this.paused ? 'play' : 'pause']();
            });
            $popupForm.find('video').on('click', function () {
                this[this.paused ? 'play' : 'pause']();
            }).trigger('click');
        }
    });
    $closePopup.on('click', function (e) {
        e.preventDefault();

        $siteWrapper.removeClass('blockBlur');
        $popupForm.removeClass('opened');
        $popupForm.fadeOut(600);;
        $popupForm.find('video').on('click', function () {
            this['pause']();
        }).trigger('click');
        $popupForm.find('video').off('click', function () {
            this[this.paused ? 'play' : 'pause']();
        });
    });

    // маска для телефонов
    $('input[type=tel]').mask("+7 (999) 999 - 99 - 99");
    if (!$('#myCanvas').tagcanvas({
        textColour: '#fff',
        outlineColour: 'rgba(255,255,255,0)',
        reverse: true,
        depth: .8,
        textHeight: 27,
        initial: [0.2, -0.1],
        zoomMax: 1,
        zoomMin: 1,
        wheelZoom: false,
        maxSpeed: 0.02,
        minSpeed: 0.01,
        textAlign: "right"
    }, 'tags')) {
        // something went wrong, hide the canvas container
        $('#myCanvasContainer').hide();
    }

    !$('#myCanvas');
    $('.b-mouse__button').on('click', function (event) {
        event.preventDefault();
        var positiionNextBlock = $(this).closest('.site-block').next('*').offset().top;
        var logoHeight = $('.app-header__logo').height();

        positiionNextBlock = positiionNextBlock - logoHeight;

        $('html, body').animate({ 'scrollTop': positiionNextBlock }, 700);
    });
    // наполняем блок навигации
    if ($('.b-navigation').length) {
        var navigationWrap = $('.b-navigation');

        $('.js-navigation').each(function (index, el) {
            navigationWrap.append('<a href="#' + $(this).attr('id') + '" class="b-navigation__item" data-title="' + $(this).data('title') + '"></a>');
        });
    }

    // активный пункт при прокрутке
    var nav = $('.b-navigation__item'),
        k = true,
        hash;
    $(window).scroll(function () {
        if (k && $(window).width() >= 1024) {
            for (var i = 0; i < nav.length; i++) {
                hash = nav.eq(i).attr('href');
                if (hash.split('#').length > 1) {
                    if ($(this).scrollTop() >= $(hash).offset().top - $('.app-header').outerHeight() - 80) {
                        nav.removeClass('js-active');
                        nav.eq(i).addClass('js-active');
                    };
                };
            };
        }
    }).trigger('scroll');

    // прокрутка до блока при клике
    $(document).on('click', '.b-navigation__item', function (event) {
        event.preventDefault();
        var scrollBlock = $(this).attr('href');
        var scrollBlockOffset = $(scrollBlock).offset().top;

        $('body, html').animate({ 'scrollTop': scrollBlockOffset }, 500);
    });
});