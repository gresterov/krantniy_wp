<?php get_header(); ?>

<div class="b-eror-block">
    <div class="b-eror-block__in container">
        <div class="b-eror-block__name">404</div>

        <!-- <= name -->
        <div class="b-eror-block__description">
            <?php if( pll_current_language() == "ru" ) : ?>
              Ошибка 404. Страница не найдена...
            <?php else : ?>
              Error 404. Page not found ...
            <?php endif; ?>
        </div>

        <!-- <= description -->
        <div class="b-eror-block__btn">
            <a href="<?= pll_home_url(); ?>" class="button-1">
                <?php if( pll_current_language() == "ru" ) : ?>
                  на главную
                <?php else : ?>
                  to home
                <?php endif; ?>
            </a>
        </div>

      <!-- <= btn -->
    </div>

    <!-- <= in -->
</div> <!-- /b-eror-block -->

<?php get_footer(); ?>
